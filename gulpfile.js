var 
	gulp     = require("gulp"),
	gutil    = require("gulp-util"),
	colors   = require("colors"),
	plumber  = require("gulp-plumber"),
	notify   = require("gulp-notify"),
	
	stylus   = require("gulp-stylus"),
	jade     = require("gulp-jade"),
	babel    = require("gulp-babel"),

	minify   = require("gulp-minify"),
	image	 = require("gulp-image"),
	concat   = require("gulp-concat"),
	html	 = require('gulp-html-prettify'),

	csscomb  = require("gulp-csscomb"),
	conccss  = require("gulp-concat-css"),
	cssmin   = require("gulp-minify-css"),
	prefixer = require("gulp-autoprefixer");




gulp.task('stylus', () => {

	gulp.src('source/stylus/main.styl')
		.pipe( plumber()  )
		.pipe( stylus()   )
		.pipe( prefixer() )
		.pipe( csscomb()  )
		.pipe( notify({title: "STYLUS", message: "Styles transpiled successfully"}))
		.pipe( gulp.dest( 'dev/css' ) );

});



gulp.task('jade', () => {

	gulp.src('source/jade/*.jade')
		.pipe( plumber() )
		.pipe( jade({ pretty: true }) )
		.pipe( html({indent_char: ' ', indent_size: 4}) )
		.pipe( notify({title: "JADE", message: "Pages transpiled successfully"}))
		.pipe( gulp.dest( 'dev/' ) ); 

});



gulp.task('babel', () => {

	gulp.src('source/js/main.js')
		.pipe( plumber() )
		.pipe( babel({presets: ['es2015']}) )
		.pipe( notify({title: "JavaScript", message: "Scripts transpiled successfully"}))
		.pipe( gulp.dest( 'dev/js' ) );

});


gulp.task('makelibs', () => {

	gulp.src(['static/libs-js/jquery*.js','static/libs-js/*.js'])
		.pipe( plumber() )
		.pipe( concat('libs.js') )
		.pipe( notify({title: "JS LIBS", message: "JS libs concatenated successfully"}))
		.pipe( gulp.dest( 'dev/js' ) );

	gulp.src('static/libs-css/*.css')
		.pipe( plumber() ) 
		.pipe( conccss('libs.css') )
		.pipe( notify({title: "CSS LIBS", message: "CSS libs concatenated successfully"}))
		.pipe( gulp.dest('dev/css') );

});


gulp.task('static', () => {

	gulp.src('static/fonts/*').pipe( gulp.dest('dev/fonts/') );
	gulp.src('static/img/**/*')
		.pipe( image({
			pngquant: true,
			optipng: true,
			zopflipng: true,
			advpng: true,
			jpegRecompress: true,
			jpegoptim: true,
			mozjpeg: true,
			gifsicle: true,
			svgo: true
	    }))
		.pipe( gulp.dest('dev/img/') );

});



gulp.task('dev_build', ['stylus','jade','babel', 'makelibs', 'static']);


gulp.task('build', () => {

	gulp.src(['dev/js/libs.js', 'dev/js/*.js'])
		.pipe( plumber() )
		// .pipe( concat('main.js') )
		.pipe( minify() )
		.pipe( gulp.dest( 'www/js' ) );

	gulp.src(['dev/css/libs.css', 'dev/css/*.css'])
		// .pipe( conccss('main.css') )
		.pipe( cssmin() )
		.pipe( gulp.dest( 'www/css' ) );

	gulp.src('dev/*.html')
		.pipe( gulp.dest('www/') )

	gulp.src('dev/fonts/*').pipe( gulp.dest('www/fonts/') );
	gulp.src('dev/img/**/*').pipe( gulp.dest('www/img/') );

});


gulp.task('watch', () => {

	gulp.watch('source/stylus/**/*.styl', ['stylus']);
	gulp.watch('source/jade/**/*.jade'  , ['jade'  ]);
	gulp.watch('source/js/**/*.js'      , ['babel' ]);
	gulp.watch('static//**/*'           , ['static' ]);

});



gulp.task('default', ['dev_build', 'watch']);